import {addUserIfNotExists, printUsers, randomInvite, removeUserFromWaitingList} from "./service/db_users.mjs";
import {Bot} from "grammy";
const token = '5797067852:AAH64nToAYrdRYMFznVFiLUdcPYRwLq5BRM';
const bot = new Bot(token);

bot.command('start', (ctx) => {
    ctx.reply(`Доброго пожаловать!
Чтобы попасть в сообщество, тебе нужно поучастовать в рулетке!
Для этого введи /invite`)
});

bot.command('invite', (ctx) => {
    let userID = ctx.message.from.id;
    let userName = ctx.message.from.username;
    let date = new Date(ctx.message.date * 1000);
    let formatData = `${date.getDate()}-${date.getMonth() + 1}-${date.getFullYear()}`

    addUserIfNotExists(userName, userID, formatData).then(() => {
        ctx.reply(`
Ты успешно записан в очередь!
Рулетка проходит каждую среду, удачи!
Если выпадет твоя очередь, я сообщу.`);
    }).catch((err) => {
        ctx.reply(`
Ты уже в очереди!`);
    })
});

bot.on('message', (ctx) => ctx.reply());
const currentDate = new Date();
if (currentDate.getDay() === 6) {
    randomInvite().then((el) => {
        bot.api.sendMessage(el, `Добро пожаловать домой!
      Заходи осваиваться в сообщесте
      https://t.me/aqulive (example)`);
      
        removeUserFromWaitingList(el).then(() => {
          console.log('Данные успешно удалены из таблицы users');
        }).catch((error) => {
          console.error('Ошибка удаления данных: ' + error.message);
        });
      });
} else {
    console.log('Сегодня не среда');
}
bot.start();