import sqlite3 from 'sqlite3'

// Создание подключения к базе данных waiting
const dbWaiting = new sqlite3.Database('./data/waiting.db', sqlite3.OPEN_READWRITE | sqlite3.OPEN_CREATE, (err) => {
    if (err) return console.log('Ошибка БД: ' + err.message);
    console.log('Успешное подключение к БД waiting');

    // Создание таблицы, если она не существует
    dbWaiting.run('CREATE TABLE IF NOT EXISTS waiting (name TEXT, userid INTEGER, data TEXT)', (err) => {
        if (err) console.error('Ошибка создания таблицы: ' + err.message);
        else {
            console.log('Таблица успешно создана.');
        }
    });
});
// Создание подключения к базе данных users
    const dbUsers = new sqlite3.Database('./data/users.db', sqlite3.OPEN_READWRITE | sqlite3.OPEN_CREATE, (err) => {
        if (err) return console.log('Ошибка БД: ' + err.message);
        console.log('Успешное подключение к  БД users');

        // Создание таблицы, если она не существует
        dbUsers.run('CREATE TABLE IF NOT EXISTS users (name TEXT, userid INTEGER, data TEXT)', (err) => {
            if (err) console.error('Ошибка создания таблицы: ' + err.message);
            else {
                console.log('Таблица успешно создана.');
            }
        });
    });


    const checkUserExists = (userid) => {
    return new Promise((resolve, reject) => {
        dbWaiting.get('SELECT * FROM waiting WHERE userid = ?', [userid], (err, row) => {
            if (err) {
                console.error('Ошибка выполнения запроса: ' + err.message);
                reject(err);
            } else {
                resolve(row !== undefined);
            }
        });
    });
};
const addUserIfNotExists = async (name, userid, data) => {
    return new Promise(async (resolve, reject) => {
        try {
            const userExists = await checkUserExists(userid);
            if (!userExists) {
                dbWaiting.run('INSERT INTO waiting (name, userid, data) VALUES (?, ?, ?)', [name, userid, data], (err) => {
                    if (err) {
                        console.error(err.message);
                        reject(err);
                    } else {
                        console.log('Данные успешно добавлены в таблицу.');
                        resolve();
                    }
                });
            } else {
                console.log('Пользователь с таким идентификатором уже существует.');
                reject();
            }
        } catch (error) {
            console.error(error);
            reject(error);
        } finally {
            // Закрытие подключения к базе данных
            // db.close();
        }
    });
};


const printUsers = () => {
    return new Promise((resolve, reject) => {
        dbWaiting.all('SELECT * FROM waiting', (err, rows) => {
            if (err) {
                console.error('Ошибка выполнения запроса: ' + err.message);
                reject(err);
            } else {
                const users = rows.map((row) => {
                    return {
                        name: row.name,
                        userid: row.userid,
                        data: row.data
                    };
                });
                resolve(users);
            }

            // Закрытие подключения к базе данных
            // db.close();
        });
    });
};

function getRandomElement(array) {
    const randomIndex = Math.floor(Math.random() * array.length);
    return array[randomIndex];
}

const removeUserFromWaitingList = (id) => {
    return new Promise((resolve, reject) => {
        dbUsers.serialize(() => {
            // Создание подключения к базе данных users

            // Выполнение запроса SELECT для получения данных
            dbWaiting.all('SELECT * FROM waiting WHERE userid = ?', [`${id}`], (err, rows) => {
                if (err) console.error('Ошибка выполнения запроса SELECT: ' + err.message);

                // Вставка данных в таблицу users
                rows.forEach((row) => {
                    dbUsers.run('INSERT INTO users (name, userid, data) VALUES (?, ?, ?)', [row.name, row.userid, row.data], (err) => {
                        if (err) console.error('Ошибка вставки данных: ' + err.message);
                    });
                });
                dbWaiting.run('DELETE FROM waiting WHERE userid = ?', [`${id}`], (err) => {
                    if (err) {
                        console.error('Ошибка удаления данных: ' + err.message);
                    }
                    else {
                        console.log('Данные успешно удалены из таблицы users');
                    } 
                });

            });

        });
    })
}
const randomInvite = () => {
    return new Promise((resolve, reject) => {
        dbWaiting.all('SELECT userid FROM waiting', (err, rows) => {
            if (err) {
                console.error('Ошибка выполнения запроса: ' + err.message);
                reject(err);
            } else {
                const users = rows.map((row) => {
                    return {
                        userid: row.userid,
                    };
                });
                if (users.length > 0) {
                    let randomUser = getRandomElement(users).userid;
                    resolve(randomUser)
                }
                
            }
        });
    });
    // const randomUserId = getRandomElement(allUserIds);
}

export {
        addUserIfNotExists,
        printUsers,
        randomInvite,
        removeUserFromWaitingList,
};